<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style type="text/css">
	
	ul {
	list-style-type: none;
	margin: 20px;
	padding: 0;
	
}

li {
	display: block;
	width: 250px;
}

</style>
</head>
<body>
<div class="row">
	<div class="col-4">
		<ul>
			<li><a href="add-user">Add user</a></li>
			
			<li class="sidebar-search">
				<form:form method="post" action="search-user" modelAttribute="u">
					<form:input path="firstName" placeholder="search" /><br/>
					<button class="btn btn-primary" type="submit" value="search">search user</button>
				</form:form>
			</li>
			
			<li>
				<form:form method="post" action="search-locker" modelAttribute="l">
					<form:input path="lockerNumber" placeholder="search " /><br/>
					<button class="btn btn-primary" type="submit" value="search">search locker</button>
				</form:form>		
			</li>
		
		</ul>
	</div>
		
	<div class="col-8">		
				<c:if test="${searchUser eq true }">
					<c:choose>
						<c:when test="${ resVide }">Aucun resultat</c:when>
						<c:otherwise>
								<%@include file="listUsers.jsp"%>							
						</c:otherwise>
					</c:choose>
				</c:if>
				
				<c:if test="${searchLocker eq true }">
					<c:choose>
						<c:when test="${ resVide }">Aucun resultat</c:when>
						<c:otherwise>
								<%@include file="listLocker.jsp"%>							
						</c:otherwise>
					</c:choose>
				</c:if>					
	</div>
</div>
</body>
</html>