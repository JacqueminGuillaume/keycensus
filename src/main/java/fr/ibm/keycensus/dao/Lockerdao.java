package fr.ibm.keycensus.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;

import fr.ibm.keycensus.beans.Locker;
import fr.ibm.keycensus.beans.Users;

public class Lockerdao {
	
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	public List<Locker> findAll(){
		return (List<Locker>) hibernateTemplate.find("From Locker", null);
	}

}
