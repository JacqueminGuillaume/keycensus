package fr.ibm.keycensus.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Builder
@Getter
@Setter
public class Users {
	
	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(length=150, nullable=false)
	private String firstName;
	
	@Column(length=150)
	private String lastName;
		
	@Column
	private String entity;
			
	@Version
	private int version; 
	
	@ManyToOne
	private Locker locker;
	
	
	public Users() {
		super();
	}

	public Users(int id, String firstName, String lastName, String entity, int version) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.entity = entity;
		this.version = version;
	}

	public Users(int id, String firstName, String lastName, String entity, int version, Locker locker) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.entity = entity;
		this.version = version;
		this.locker = locker;
	}

	
	
	
	
	
	

}
