package fr.ibm.keycensus.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Builder
@Getter
@Setter
public class Locker {
 
	
	@Id
	@Column(nullable=false)
	private String LockerNumber;
	
	@Column
	private int KeyNumber;
	

	@OneToMany(mappedBy="locker", cascade=CascadeType.ALL)
	private List<Users> ListUserLocker;

	
	public Locker() {
		super();
	}

	public Locker(String lockerNumber, int keyNumber, List<Users> listUserLocker) {
		super();
		LockerNumber = lockerNumber;
		KeyNumber = keyNumber;
		ListUserLocker = listUserLocker;
	}


	
	
	

}
