<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h3>Ajout</h3>
<br/>
<form:form method="post" action="save-user" modelAttribute="u" >

		<form:label path="lastName">lastname: </form:label>
		<form:input path="lastName" />
		<br />

		<form:label path="firstName">firstname: </form:label>
		<form:input path="firstName" />
		<br />

		<form:label path="entity">entity : </form:label>
		<form:input path="entity" />
		<br />
		
		<form:label path="locker.lockerNumber">Attribute locker : </form:label>		
			<form:select path="locker.lockerNumber">
				<form:option value=""  label="--Select locker"/>
				<form:options items="${listeLocker}"/>
						
			</form:select>
				
		<br/>
		<form:hidden path="id" />
		<form:hidden path="version" />
		<input type="submit" value="Ajouter" />
</form:form>

</body>
</html>