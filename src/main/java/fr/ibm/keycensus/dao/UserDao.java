package fr.ibm.keycensus.dao;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.ibm.keycensus.beans.Users;

public class UserDao {
	private HibernateTemplate hibernateTemplate;

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	public List<Users> findAll(){
		return (List<Users>) hibernateTemplate.find("From Users", null);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Users> findByName(String rech) {
		return hibernateTemplate
				.getSessionFactory()
				.getCurrentSession()
				.createQuery("FROM Users u WHERE u.firstName LIKE :rech")
				.setParameter("rech", "%" + rech + "%")
				.list();
	}
	
	@Transactional
	public void insert(Users u) {
		 hibernateTemplate.save(u);
	} 
	
	@Transactional
	public void update(Users u) {
		hibernateTemplate.saveOrUpdate(u);
		
	}
	
	@Transactional(readOnly=true)
	public Users findUser(int id){
		SessionFactory sessionFactory = hibernateTemplate.getSessionFactory();
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Users where id= :id");
		
		query.setParameter("id", id);
		
		
		Users user = (Users) query.uniqueResult();
		
		return user;	
	}
	
	@Transactional
	public void remove(int id) {
		hibernateTemplate.delete(findUser(id));
	}
	
	

}
