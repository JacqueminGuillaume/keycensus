package fr.ibm.keycensus.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.ibm.keycensus.beans.Locker;
import fr.ibm.keycensus.beans.Users;
import fr.ibm.keycensus.dao.Lockerdao;
import fr.ibm.keycensus.dao.UserDao;


@Controller
public class HomeController {
	
	@Autowired
	private UserDao userdao;
	
	@Autowired
	private Lockerdao lockerdao;
		
	@GetMapping("/")
	public String home(Locale locale, Model model) {
		model.addAttribute("u", new Users());
		model.addAttribute("l", new Locker());
		return "home";
	}

	@PostMapping("/search-user")
	public String searchUser(Model model, @ModelAttribute("u") Users u, BindingResult result) {
		List<Users> lu = userdao.findByName(u.getFirstName());
		model.addAttribute("users", lu);
		model.addAttribute("u", new Users());
		model.addAttribute("u", u);
		model.addAttribute("l", new Locker());
		model.addAttribute("resVide", (lu == null || lu.size() == 0));
		model.addAttribute("searchUser", true);
		return "home";
	}
	
	@PostMapping("/search-locker")
	public String searchUser(Model model, @ModelAttribute("l") Locker l, BindingResult result) {
		List<Locker> listLocker = lockerdao.findAll();
		model.addAttribute("lockers", listLocker);
		model.addAttribute("l", new Locker());
		model.addAttribute("l", l);
		model.addAttribute("u", new Users());
		model.addAttribute("resVide", (listLocker == null || listLocker.size() == 0));
		model.addAttribute("searchLocker", true);
		return "home";
	}
	
	@GetMapping("/add-user")
	public String addUser(Model model) {
		List<String> listLockerNumber = getLockerNumberByFindAll();
		
		model.addAttribute("listeLocker", listLockerNumber);
		model.addAttribute("locker", new Locker());
		model.addAttribute("u", new Users());
		return "user-form";
	}


	
	@PostMapping("/save-user")
	public String saveUser(@ModelAttribute("u") Users u, BindingResult result) {
			
		userdao.insert(u);				
		return "redirect:/";
	} 
	
	@GetMapping("/delete/{id}")
	public String deleteUser( @PathVariable("id") int id) {
		
		userdao.remove(id);		
		return "redirect:/";
		
	}
	
	@GetMapping("/update/{id}")
	public String modifyUser( @PathVariable("id") int id, Model model) {
			Users u = userdao.findUser(id);
			model.addAttribute("u", u);	
			List<String> listLockerNumber = getLockerNumberByFindAll();
			model.addAttribute("listeLocker", listLockerNumber);
		return "updateUser";
	}
	
	@PostMapping("/update/update-user")
	public String updateUser(@ModelAttribute("u") Users u, BindingResult result, Model model) {
		userdao.update(u);
		model.addAttribute("locker", new Locker());
		return "redirect:/";
	} 
	
	
	private List<String> getLockerNumberByFindAll() {
		List<Locker> lockers= lockerdao.findAll();
		
		return lockers.stream().map(locker -> locker.getLockerNumber()).collect(Collectors.toList());

		
	}
	
	
	
	

	
	
}
